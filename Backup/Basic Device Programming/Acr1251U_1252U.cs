﻿/*===========================================================================================
 * 
 *  Author          : Nabs P. Nabus
 * 
 *  File            : Acr1251U_1252U.cs
 * 
 *  Copyright (C)   : Advanced Card System Ltd.
 * 
 *  Description     : Contains Methods and Properties for ACR1251U and ACR1252U SmartCard Reader
 * 
 *  Date            : March 13, 2014
 * 
 *  Revision Traile : [Author] / [Date of modification] / [Details of Modifications done]
 * 
 * 
 * =========================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acs;
using Acs.Readers.Pcsc;

namespace UsbNfcBasicDeviceProgramming
{
    public enum LED_STATE
    {
        ON = 0x01,
        OFF = 0x00
    }

    public enum LED_BUZZER_BEHAVIOR
    { 
        ENABLED = 0x01,
        DISABLED = 0x00
    }

    public enum POLLING_INTERVAL
    { 
        MS_250,
        MS_500,
        MS_1000,
        MS_2500        
    }

    public enum POLLING_SETTING
    { 
        ENABLED = 0x01,
        DISABLED = 0x00
    }
    
    public class LedStatus
    {
        private LED_STATE _red = LED_STATE.OFF;
        private LED_STATE _green = LED_STATE.OFF;

        public LedStatus()
        {

        }

        public LedStatus(byte ledStatus)
        {
            if ((ledStatus & 0x01) == 0x01)
                _red = LED_STATE.ON;

            if ((ledStatus & 0x02) == 0x02)
                _green = LED_STATE.ON;
        }

        public LED_STATE red
        {
            get { return _red; }
            set { _red = value; }
        }

        public LED_STATE green
        {
            get { return _green; }
            set { _green = value; }
        }

        public byte getRawLedStatus()
        {
            byte ledStatus = 0x00;

            if (red == LED_STATE.ON)
                ledStatus |= 0x01;

            if (green == LED_STATE.ON)
                ledStatus |= 0x02;

            return ledStatus;
        }
    }

    public class PiccOperatingParameter
    {        

        public enum PARAMETER_OPTION
        {
            SKIP = 0x00,
            DETECT = 0x01
        }

        private PARAMETER_OPTION _iso14443TypeA = PARAMETER_OPTION.SKIP;
        private PARAMETER_OPTION _iso14443TypeB = PARAMETER_OPTION.SKIP;
        private PARAMETER_OPTION _felica212 = PARAMETER_OPTION.SKIP;
        private PARAMETER_OPTION _felica424 = PARAMETER_OPTION.SKIP;
        private PARAMETER_OPTION _topaz = PARAMETER_OPTION.SKIP;

        public PiccOperatingParameter()
        { 
        
        }

        public PiccOperatingParameter(byte rawOperatingParameter)
        {
            if ((rawOperatingParameter & 0x01) == 0x01)
                iso14443TypeA = PARAMETER_OPTION.DETECT;

            if ((rawOperatingParameter & 0x02) == 0x02)
                iso14443TypeB = PARAMETER_OPTION.DETECT;

            if ((rawOperatingParameter & 0x04) == 0x04)
                felica212 = PARAMETER_OPTION.DETECT;

            if ((rawOperatingParameter & 0x08) == 0x08)
                felica424 = PARAMETER_OPTION.DETECT;

            if ((rawOperatingParameter & 0x10) == 0x10)
                topaz = PARAMETER_OPTION.DETECT;
        }

        public PARAMETER_OPTION iso14443TypeA
        {
            get { return _iso14443TypeA; }
            set { _iso14443TypeA = value; }
        }

        public PARAMETER_OPTION iso14443TypeB
        {
            get { return _iso14443TypeB; }
            set { _iso14443TypeB = value; }
        }

        public PARAMETER_OPTION felica212
        {
            get { return _felica212; }
            set { _felica212 = value; }
        }

        public PARAMETER_OPTION felica424
        {
            get { return _felica424; }
            set { _felica424 = value; }
        }

        public PARAMETER_OPTION topaz
        {
            get { return _topaz; }
            set { _topaz = value; }
        }

        public byte getRawOperatingParameter()
        {
            byte operatingParameter = 0x00;

            if (iso14443TypeA == PARAMETER_OPTION.DETECT)
                operatingParameter |= 0x01;

            if (iso14443TypeB == PARAMETER_OPTION.DETECT)
                operatingParameter |= 0x02;

            if (felica212 == PARAMETER_OPTION.DETECT)
                operatingParameter |= 0x04;

            if (felica424 == PARAMETER_OPTION.DETECT)
                operatingParameter |= 0x08;

            if (topaz == PARAMETER_OPTION.DETECT)
                operatingParameter |= 0x10;

            return operatingParameter;
        }
    }

    public class LedBuzzerBehaviorPicc
    {
        private LED_BUZZER_BEHAVIOR _piccCardOperationBlinkingLed = LED_BUZZER_BEHAVIOR.DISABLED;
        private LED_BUZZER_BEHAVIOR _piccPollingLed = LED_BUZZER_BEHAVIOR.DISABLED;
        private LED_BUZZER_BEHAVIOR _piccActivationLed = LED_BUZZER_BEHAVIOR.DISABLED;
        private LED_BUZZER_BEHAVIOR _cardInsertionRemovalBuzzer = LED_BUZZER_BEHAVIOR.DISABLED;
        private LED_BUZZER_BEHAVIOR _pn512ResetIndicationBuzzer = LED_BUZZER_BEHAVIOR.DISABLED;
        private LED_BUZZER_BEHAVIOR _piccColorSelectRed = LED_BUZZER_BEHAVIOR.DISABLED;
        private LED_BUZZER_BEHAVIOR _piccColorSelectGreen = LED_BUZZER_BEHAVIOR.DISABLED;

        public LedBuzzerBehaviorPicc()
        {
        
        }

        public LedBuzzerBehaviorPicc(byte behaviorSettings)
        {
            if ((behaviorSettings & 0x01) == 0x01)
                _piccCardOperationBlinkingLed = LED_BUZZER_BEHAVIOR.ENABLED;

            if ((behaviorSettings & 0x02) == 0x02)
                _piccPollingLed = LED_BUZZER_BEHAVIOR.ENABLED;

            if ((behaviorSettings & 0x04) == 0x04)
                _piccActivationLed = LED_BUZZER_BEHAVIOR.ENABLED;

            if ((behaviorSettings & 0x08) == 0x08)
                _cardInsertionRemovalBuzzer = LED_BUZZER_BEHAVIOR.ENABLED;

            if ((behaviorSettings & 0x20) == 0x20)
                _pn512ResetIndicationBuzzer = LED_BUZZER_BEHAVIOR.ENABLED;

            if ((behaviorSettings & 0x40) == 0x40)
                _piccColorSelectGreen = LED_BUZZER_BEHAVIOR.ENABLED;

            if ((behaviorSettings & 0x80) == 0x80)
                _piccColorSelectRed = LED_BUZZER_BEHAVIOR.ENABLED;
        }

        public LED_BUZZER_BEHAVIOR piccCardOperationBlinkingLed
        {
            get { return _piccCardOperationBlinkingLed; }
            set { _piccCardOperationBlinkingLed = value; }
        }

        public LED_BUZZER_BEHAVIOR piccPollingLed
        {
            get { return _piccPollingLed; }
            set { _piccPollingLed = value; }
        }

        public LED_BUZZER_BEHAVIOR piccActivationLed
        {
            get { return _piccActivationLed; }
            set { _piccActivationLed = value; }
        }

        public LED_BUZZER_BEHAVIOR cardInsertionRemovalBuzzer
        {
            get { return _cardInsertionRemovalBuzzer; }
            set { _cardInsertionRemovalBuzzer = value; }
        }

        public LED_BUZZER_BEHAVIOR pn512ResetIndicationBuzzer
        {
            get { return _pn512ResetIndicationBuzzer; }
            set { _pn512ResetIndicationBuzzer = value; }
        }

        public LED_BUZZER_BEHAVIOR piccColorSelectRed
        {
            get { return _piccColorSelectRed; }
            set { _piccColorSelectRed = value; }
        }

        public LED_BUZZER_BEHAVIOR piccColorSelectGreen
        {
            get { return _piccColorSelectGreen; }
            set { _piccColorSelectGreen = value; }
        }

        public byte getRawLedBuzzerBehaviorPicc()
        {
            byte ledBuzzerBehavior = 0x00;

            if (piccCardOperationBlinkingLed == LED_BUZZER_BEHAVIOR.ENABLED)
                ledBuzzerBehavior |= 0x01;

            if (piccPollingLed == LED_BUZZER_BEHAVIOR.ENABLED)
                ledBuzzerBehavior |= 0x02;

            if (piccActivationLed == LED_BUZZER_BEHAVIOR.ENABLED)
                ledBuzzerBehavior |= 0x04;

            if (cardInsertionRemovalBuzzer == LED_BUZZER_BEHAVIOR.ENABLED)
                ledBuzzerBehavior |= 0x08;

            if (pn512ResetIndicationBuzzer == LED_BUZZER_BEHAVIOR.ENABLED)
                ledBuzzerBehavior |= 0x20;

            if (piccColorSelectGreen == LED_BUZZER_BEHAVIOR.ENABLED)
                ledBuzzerBehavior |= 0x40;

            if (piccColorSelectRed == LED_BUZZER_BEHAVIOR.ENABLED)
                ledBuzzerBehavior |= 0x80;

            return ledBuzzerBehavior;
        }
    }

    public class AutoPiccPollingSettings
    {
        public AutoPiccPollingSettings()
        { 
        
        }

        public AutoPiccPollingSettings(byte rawPiccPollingSetting)
        {
            setRawPollingSetting(rawPiccPollingSetting);
        }

        private POLLING_SETTING _autoPiccPolling = POLLING_SETTING.DISABLED;
        private POLLING_SETTING _turnOffAntennaIfNoPicc = POLLING_SETTING.DISABLED;
        private POLLING_SETTING _turnOffAntennaIfPiccInactive = POLLING_SETTING.DISABLED;
        private POLLING_SETTING _activatePiccWhenDetected = POLLING_SETTING.DISABLED;
        private POLLING_SETTING _enforceIso14443APart4 = POLLING_SETTING.DISABLED;
        private POLLING_INTERVAL _pollingInterval = POLLING_INTERVAL.MS_250;

        public POLLING_SETTING autoPiccPolling
        {
            get { return _autoPiccPolling; }
            set { _autoPiccPolling = value; }
        }

        public POLLING_SETTING turnOffAntennaIfNoPicc
        {
            get { return _turnOffAntennaIfNoPicc; }
            set { _turnOffAntennaIfNoPicc = value; }
        }

        public POLLING_SETTING turnOffAntennaIfPiccInactive
        {
            get { return _turnOffAntennaIfPiccInactive; }
            set { _turnOffAntennaIfPiccInactive = value; }
        }

        public POLLING_SETTING activatePiccWhenDetected
        {
            get { return _activatePiccWhenDetected; }
            set { _activatePiccWhenDetected = value; }
        }

        public POLLING_SETTING enforceIso14443APart4
        {
            get { return _enforceIso14443APart4; }
            set { _enforceIso14443APart4 = value; }
        }

        public POLLING_INTERVAL pollingInterval
        {
            get { return _pollingInterval; }
            set { _pollingInterval = value; }
        }

        public void setRawPollingSetting(byte rawPollingSetting)
        {
            if ((rawPollingSetting & 0x01) == 0x01)
                autoPiccPolling = POLLING_SETTING.ENABLED;

            if ((rawPollingSetting & 0x02) == 0x02)
                turnOffAntennaIfNoPicc = POLLING_SETTING.ENABLED;

            if ((rawPollingSetting & 0x04) == 0x04)
                turnOffAntennaIfPiccInactive = POLLING_SETTING.ENABLED;

            if ((rawPollingSetting & 0x08) == 0x08)
                activatePiccWhenDetected = POLLING_SETTING.ENABLED;

            if ((rawPollingSetting & 0x80) == 0x80)
                enforceIso14443APart4 = POLLING_SETTING.ENABLED;

            switch ((rawPollingSetting >> 4) & 0x03)
            { 
                case 0:
                    pollingInterval = POLLING_INTERVAL.MS_250;
                    break;
                case 1:
                    pollingInterval = POLLING_INTERVAL.MS_500;
                    break;
                case 2:
                    pollingInterval = POLLING_INTERVAL.MS_1000;
                    break;
                case 3:
                    pollingInterval = POLLING_INTERVAL.MS_2500;
                    break;
                default:
                    pollingInterval = POLLING_INTERVAL.MS_250;
                    break;
            }
        }

        public byte getRawPollingSettings()
        {
            byte rawPollingSetting = 0x00;

            if (autoPiccPolling == POLLING_SETTING.ENABLED)
                rawPollingSetting |= 0x01;

            if (turnOffAntennaIfNoPicc == POLLING_SETTING.ENABLED)
                rawPollingSetting |= 0x02;

            if (turnOffAntennaIfPiccInactive == POLLING_SETTING.ENABLED)
                rawPollingSetting |= 0x04;

            if (activatePiccWhenDetected == POLLING_SETTING.ENABLED)
                rawPollingSetting |= 0x08;

            if (enforceIso14443APart4 == POLLING_SETTING.ENABLED)
                rawPollingSetting |= 0x80;

            switch (pollingInterval)
            {
                case POLLING_INTERVAL.MS_2500:
                    rawPollingSetting |= 0x30; 
                    break;
                case POLLING_INTERVAL.MS_1000:
                    rawPollingSetting |= 0x20;
                    break;
                case POLLING_INTERVAL.MS_500:
                    rawPollingSetting |= 0x10; 
                    break;
                default:
                    rawPollingSetting |= 0x00; 
                    break;
            }

            return rawPollingSetting;
        }
    }

    public class Acr1251U_1252U : PcscReader
    {
        public Acr1251U_1252U()
        {
            operationControlCode = (uint)PcscProvider.FILE_DEVICE_SMARTCARD + 3500 * 4;
        }

        public Acr1251U_1252U(string selectedReader)
        {
            readerName = selectedReader;
            operationControlCode = (uint)PcscProvider.FILE_DEVICE_SMARTCARD + 3500 * 4;
        }

        public override byte[] getFirmwareVersion()
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 256;
            apdu.data = new byte[5];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x18;
            apdu.data[4] = 0x00;

            apduCommand = apdu;
            sendCardControl();

            return apduCommand.response;
        }

        public LedStatus setLedStatus(LedStatus ledStatus)
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 6;
            apdu.data = new byte[6];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x29;
            apdu.data[4] = 0x01;
            apdu.data[5] = ledStatus.getRawLedStatus();

            apduCommand = apdu;
            sendCardControl();

            return new LedStatus(apduCommand.response[5]);
        }

        public LedStatus getLedStatus()
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 6;
            apdu.data = new byte[5];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x29;
            apdu.data[4] = 0x00;

            apduCommand = apdu;
            sendCardControl();

            return new LedStatus(apduCommand.response[5]);
        }

        public void setBuzzerStatus(byte duration)
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 6;
            apdu.data = new byte[6];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x28;
            apdu.data[4] = 0x01;
            apdu.data[5] = duration;
          
            apduCommand = apdu;
            sendCardControl();
        }

        public byte getBuzzerStatus()
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 6;
            apdu.data = new byte[5];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x28;
            apdu.data[4] = 0x00;

            apduCommand = apdu;
            sendCardControl();

            return apduCommand.response[5];
        }

        public LedBuzzerBehaviorPicc readLedBuzzerBehaviorPicc()
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 6;
            apdu.data = new byte[5];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x21;
            apdu.data[4] = 0x00;

            apduCommand = apdu;
            sendCardControl();

            return new LedBuzzerBehaviorPicc(apduCommand.response[5]);
        }

        public LedBuzzerBehaviorPicc setLedBuzzerBehaviorPicc(LedBuzzerBehaviorPicc behaviorSettings)
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 6;
            apdu.data = new byte[6];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x21;
            apdu.data[4] = 0x01;
            apdu.data[5] = behaviorSettings.getRawLedBuzzerBehaviorPicc();

            apduCommand = apdu;
            sendCardControl();

            return new LedBuzzerBehaviorPicc(apduCommand.response[5]);
        }

        public PiccOperatingParameter readPiccOperatingParameter()
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 6;
            apdu.data = new byte[5];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x20;
            apdu.data[4] = 0x00;

            apduCommand = apdu;
            sendCardControl();

            return new PiccOperatingParameter(apduCommand.response[5]);
        }

        public PiccOperatingParameter setPiccOperatingParameter(PiccOperatingParameter operatingParameter)
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 6;
            apdu.data = new byte[6];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x20;
            apdu.data[4] = 0x01;
            apdu.data[5] = operatingParameter.getRawOperatingParameter();

            apduCommand = apdu;
            sendCardControl();

            return new PiccOperatingParameter(apduCommand.response[5]);
        }

        public AutoPiccPollingSettings setAutomaticPiccPolling(AutoPiccPollingSettings pollingSetting)
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 6;
            apdu.data = new byte[6];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x23;
            apdu.data[4] = 0x01;
            apdu.data[5] = pollingSetting.getRawPollingSettings();

            apduCommand = apdu;
            sendCardControl();
            return new AutoPiccPollingSettings(apduCommand.response[5]);
        }

        public AutoPiccPollingSettings readAutomaticPiccPolling()
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 6;
            apdu.data = new byte[5];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x23;
            apdu.data[4] = 0x00;           

            apduCommand = apdu;
            sendCardControl();
            return new AutoPiccPollingSettings(apduCommand.response[5]);
        }

        public void setSerialNumber(byte[] serialNumber)
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 255;
            apdu.data = new byte[serialNumber.Length + 5];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0xDA;
            apdu.data[4] = (byte)serialNumber.Length;

            Array.Copy(serialNumber, 0, apdu.data, 5, serialNumber.Length); 

            apduCommand = apdu;
            sendCardControl();
        }

        public void setAndLockSerialNumber(byte[] serialNumber)
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 255;
            apdu.data = new byte[serialNumber.Length + 5];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0xD9;
            apdu.data[4] = (byte)serialNumber.Length;

            Array.Copy(serialNumber, 0, apdu.data, 5, serialNumber.Length);

            apduCommand = apdu;
            sendCardControl();
        }

        public byte[] readSerialNumber()
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 25;
            apdu.data = new byte[5];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x33;
            apdu.data[4] = 0x00;

            apduCommand = apdu;
            sendCardControl();

            return apdu.response;
        }

        public void unlockSerialNumber()
        {
            Apdu apdu = new Apdu();
            apdu.lengthExpected = 7;
            apdu.data = new byte[5];

            apdu.data[0] = 0xE0;
            apdu.data[1] = 0x00;
            apdu.data[2] = 0x00;
            apdu.data[3] = 0x3E;
            apdu.data[4] = 0x00;

            apduCommand = apdu;
            sendCardControl();
        }
    }
}
