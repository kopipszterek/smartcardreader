﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Acs;
using Acs.Readers.Pcsc;
using System.Globalization;
using System.Text.RegularExpressions;

namespace UsbNfcBasicDeviceProgramming
{
    public partial class FormMain : Form
    {
        #region Global Variables
        Acr1251U_1252U _nfcReader;
        #endregion

        #region Helper Method
        void resetApplication()
        {
            ComboBoxReader.Items.Clear();

            if (_nfcReader != null)
            {
                try
                {
                    _nfcReader.disconnect();
                }
                catch (Exception ex)
                {
                    showSystemException("Reset failed.\r\n" + ex.Message);
                }
            }

            enableDisableGroupBox(false);

            _nfcReader = new Acr1251U_1252U();
            _nfcReader.OnReceivedCommand += new TransmitApduDelegate(NfcReaderOnReceivedCommand);
            _nfcReader.OnSendCommand += new TransmitApduDelegate(NfcReaderOnSendCommand);

            RichTextBoxLogs.Clear();
            addMsgToLog("Program Ready\r\n");

            ComboBoxReader.Enabled = true;
            ButtonInitialize.Enabled = true;
            ButtonConnect.Enabled = false;
            LabelFirmware.Text = "";

            CheckBoxRedLed.Checked = false;
            CheckBoxGreenLed.Checked = false;

            NumericUpDownDuration.Value = 0;

            CheckBoxPiccCardOperationLed.Checked = false;
            CheckBoxPiccPollingLed.Checked = false;
            CheckBoxPiccActivationLed.Checked = false;
            CheckBoxPiccCardInsertionBuzzer.Checked = false;
            CheckBoxPiccPN512ResetIndicationBuzzer.Checked = false;
            CheckBoxPiccColorSelectRed.Checked = false;
            CheckBoxPiccColorSelectGreen.Checked = false;

            CheckBoxTypeA.Checked = false;
            CheckBoxTypeB.Checked = false;
            CheckBoxFelica212.Checked = false;
            CheckBoxFelica424.Checked = false;
            CheckBoxTopaz.Checked = false;

            TextBoxSerialNumber.Text = "";
            ((TextBox)NumericUpDownDuration.Controls[1]).MaxLength = 3;
        }

        void NfcReaderOnSendCommand(object sender, TransmitApduEventArg e)
        {
            addMsgToLog("<< ", e.data, "", e.data.Length, false, false);
        }

        void NfcReaderOnReceivedCommand(object sender, TransmitApduEventArg e)
        {
            addMsgToLog(">> ", e.data, "", e.data.Length, false, true);
        }

        void addTitleToLog(bool isBold, string msg)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { addTitleToLog(isBold, msg); }));
            }
            else
            {
                if (isBold)
                    RichTextBoxLogs.SelectionFont = new Font(RichTextBoxLogs.Font.Name, RichTextBoxLogs.Font.Size, FontStyle.Bold);
                else
                    RichTextBoxLogs.SelectionFont = new Font(RichTextBoxLogs.Font.Name, RichTextBoxLogs.Font.Size, FontStyle.Regular);

                RichTextBoxLogs.Select(RichTextBoxLogs.Text.Length, 0);
                RichTextBoxLogs.SelectionColor = Color.Black;
                RichTextBoxLogs.SelectedText = msg + "\r\n";
                RichTextBoxLogs.ScrollToCaret();
            }
        }

        void addMsgToLog(string msg)
        {

            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { addMsgToLog(msg); }));
            }
            else
            {
                RichTextBoxLogs.Select(RichTextBoxLogs.Text.Length, 0);
                RichTextBoxLogs.SelectionColor = Color.Black;
                RichTextBoxLogs.SelectionFont = new Font(RichTextBoxLogs.Font.Name, RichTextBoxLogs.Font.Size, FontStyle.Regular);
                RichTextBoxLogs.SelectedText = msg + "\r\n";
                RichTextBoxLogs.ScrollToCaret();
            }
        }

        void addErrMsgToLog(string msg)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { addErrMsgToLog(msg); }));
            }
            else
            {
                RichTextBoxLogs.Select(RichTextBoxLogs.Text.Length, 0);
                RichTextBoxLogs.SelectionColor = Color.Red;
                RichTextBoxLogs.SelectionFont = new Font(RichTextBoxLogs.Font.Name, RichTextBoxLogs.Font.Size, FontStyle.Regular);
                RichTextBoxLogs.SelectedText = msg + "\r\n";
                RichTextBoxLogs.ScrollToCaret();
            }
        }

        void addMsgToLog(string prefixStr, byte[] buff, string postfixStr, int buffLen, bool isIndented, bool isSpace)
        {
            if (buff.Length < buffLen)
                return;

            RichTextBoxLogs.Select(RichTextBoxLogs.Text.Length, 0);
            if (isIndented)
                RichTextBoxLogs.SelectionIndent = 10;
            else
                RichTextBoxLogs.SelectionIndent = 0;

            string tmpStr = string.Empty;

            // Convert each byte from buff to its string representation.
            for (int i = 0; i < buffLen; i++)
                tmpStr += string.Format("{0:X2}", buff[i]) + " ";

            // Add item to log (listBox)                   
            RichTextBoxLogs.SelectionColor = Color.Green;
            RichTextBoxLogs.SelectionFont = new Font(RichTextBoxLogs.Font.Name, RichTextBoxLogs.Font.Size, FontStyle.Regular);
            if (isSpace)
                RichTextBoxLogs.SelectedText = prefixStr + tmpStr + postfixStr + "\r\n\n";
            else
                RichTextBoxLogs.SelectedText = prefixStr + tmpStr + postfixStr + "\r\n";

            // Select the last item from the listbox.
            RichTextBoxLogs.ScrollToCaret();
        }

        void enableDisableGroupBox(bool enable)
        {
            GroupBoxFirmware.Enabled = enable;
            GroupBoxLed.Enabled = enable;
            GroupBoxBuzzerControl.Enabled = enable;
            GroupBoxLedBuzzerBehaviorPicc.Enabled = enable;
            GroupBoxPiccOperatingParameter.Enabled = enable;
            GroupBoxSerialNumber.Enabled = enable;
        }

        void showPcscException(string msg)
        {
            addErrMsgToLog(msg);
            MessageBox.Show(msg, "PCSC Exception", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        void showSystemException(string msg)
        {
            addErrMsgToLog(msg);
            MessageBox.Show(msg, "System Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        void showWarningMessage(string msg)
        {
            addErrMsgToLog(msg);
            MessageBox.Show(msg, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        #endregion

        #region Form
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            resetApplication();
        }
        #endregion

        #region Button Events
        private void ButtonInitialize_Click(object sender, EventArgs e)
        {
            try
            {
                string[] readers = PcscHelper.getAllReaders();

                ComboBoxReader.Items.Clear();
                ComboBoxReader.Items.AddRange(readers);

                if (ComboBoxReader.Items.Count > 0)
                    ComboBoxReader.SelectedIndex = 0;

                ButtonConnect.Enabled = true;
                addMsgToLog("Initialize Success\r\n");
            }
            catch (PcscException ex)
            {
                showPcscException("List reader failed.\r\n" + ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException("List reader failed.\r\n" + ex.Message);
            }
        }

        private void ButtonConnect_Click(object sender, EventArgs e)
        {
            if (ComboBoxReader.SelectedIndex < 0)
            {
                showWarningMessage("Please select smartcard reader");
                return;
            }

            try
            {
                _nfcReader.readerName = ComboBoxReader.SelectedItem.ToString();
                _nfcReader.connectDirect();
                addMsgToLog("Connected to " + ComboBoxReader.SelectedItem.ToString() + "\r\n");
                enableDisableGroupBox(true);
                ButtonConnect.Enabled = false;
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonGetFirmware_Click(object sender, EventArgs e)
        {
            byte[] firmware;

            if (_nfcReader == null)
            {
                showWarningMessage("Please connect ACR1252U reader");
                return;
            }

            addTitleToLog(false, "Get Firmware Version");

            try
            {
                firmware = _nfcReader.getFirmwareVersion();
                LabelFirmware.Text = ASCIIEncoding.ASCII.GetString(firmware, 5, firmware.Length - 5);
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonSetLedStatus_Click(object sender, EventArgs e)
        {
            LedStatus ledStatus = new LedStatus();

            if (_nfcReader == null)
            {
                showWarningMessage("Please connect ACR1252U reader");
                return;
            }

            addTitleToLog(false, "Set LED Status");

            try
            {
                if (CheckBoxRedLed.Checked)
                    ledStatus.red = LED_STATE.ON;

                if (CheckBoxGreenLed.Checked)
                    ledStatus.green = LED_STATE.ON;

                ledStatus = _nfcReader.setLedStatus(ledStatus);

                CheckBoxRedLed.Checked = false;
                CheckBoxGreenLed.Checked = false;

            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonGetLedStatus_Click(object sender, EventArgs e)
        {
            LedStatus ledStatus = new LedStatus();

            if (_nfcReader == null)
            {
                showWarningMessage("Please connect ACR1252U reader");
                return;
            }

            addTitleToLog(false, "Get LED Status");

            try
            {
                ledStatus = _nfcReader.getLedStatus();

                if (ledStatus.red == LED_STATE.ON)
                    CheckBoxRedLed.Checked = true;
                else
                    CheckBoxRedLed.Checked = false;

                if (ledStatus.green == LED_STATE.ON)
                    CheckBoxGreenLed.Checked = true;
                else
                    CheckBoxGreenLed.Checked = false;
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonSetBuzzerStatus_Click(object sender, EventArgs e)
        {
            if (_nfcReader == null)
            {
                showWarningMessage("Please connect ACR1252U reader");
                return;
            }

            if (NumericUpDownDuration.Value > 255 && NumericUpDownDuration.Value < 0)
            {
                showWarningMessage("Invalid input. Please input a value from 0 - 255");
                return;
            }

            addTitleToLog(false, "Set Buzzer Status");

            try
            {
                _nfcReader.setBuzzerStatus((byte)NumericUpDownDuration.Value);
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonGetBuzzerStatus_Click(object sender, EventArgs e)
        {
            if (_nfcReader == null)
            {
                showWarningMessage("Please connect ACR1252U reader");
                return;
            }

            addTitleToLog(false, "Get Buzzer Status");

            try
            {
                NumericUpDownDuration.Value = (int)_nfcReader.getBuzzerStatus();
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonSetBehaviorPicc_Click(object sender, EventArgs e)
        {
            LedBuzzerBehaviorPicc behaviorSettings;

            if (_nfcReader == null)
            {
                showWarningMessage("Please connect ACR1252U reader");
                return;
            }

            addTitleToLog(false, "Set LED and Buzzer Behavior");

            try
            {
                behaviorSettings = new LedBuzzerBehaviorPicc();

                if(CheckBoxPiccCardOperationLed.Checked)
                    behaviorSettings.piccCardOperationBlinkingLed = LED_BUZZER_BEHAVIOR.ENABLED;

                if (CheckBoxPiccPollingLed.Checked)
                    behaviorSettings.piccPollingLed = LED_BUZZER_BEHAVIOR.ENABLED;

                if (CheckBoxPiccActivationLed.Checked)
                    behaviorSettings.piccActivationLed = LED_BUZZER_BEHAVIOR.ENABLED;

                if (CheckBoxPiccCardInsertionBuzzer.Checked)
                    behaviorSettings.cardInsertionRemovalBuzzer = LED_BUZZER_BEHAVIOR.ENABLED;

                if (CheckBoxPiccPN512ResetIndicationBuzzer.Checked)
                    behaviorSettings.pn512ResetIndicationBuzzer = LED_BUZZER_BEHAVIOR.ENABLED;

                if (CheckBoxPiccColorSelectRed.Checked)
                    behaviorSettings.piccColorSelectRed = LED_BUZZER_BEHAVIOR.ENABLED;

                if (CheckBoxPiccColorSelectGreen.Checked)
                    behaviorSettings.piccColorSelectGreen = LED_BUZZER_BEHAVIOR.ENABLED;

                behaviorSettings = _nfcReader.setLedBuzzerBehaviorPicc(behaviorSettings);

                CheckBoxPiccCardOperationLed.Checked = false;
                CheckBoxPiccPollingLed.Checked = false;
                CheckBoxPiccActivationLed.Checked = false;
                CheckBoxPiccCardInsertionBuzzer.Checked = false;
                CheckBoxPiccPN512ResetIndicationBuzzer.Checked = false;
                CheckBoxPiccColorSelectRed.Checked = false;
                CheckBoxPiccColorSelectGreen.Checked = false;
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonReadBehaviorPicc_Click(object sender, EventArgs e)
        {
            LedBuzzerBehaviorPicc behaviorSettings;

            if (_nfcReader == null)
            {
                showWarningMessage("Please connect ACR1252U reader");
                return;
            }

            addTitleToLog(false, "Read LED and Buzzer Behavior");

            try
            {
                behaviorSettings = _nfcReader.readLedBuzzerBehaviorPicc();

                if (behaviorSettings.piccCardOperationBlinkingLed == LED_BUZZER_BEHAVIOR.ENABLED)
                    CheckBoxPiccCardOperationLed.Checked = true;
                else
                    CheckBoxPiccCardOperationLed.Checked = false;

                if (behaviorSettings.piccPollingLed == LED_BUZZER_BEHAVIOR.ENABLED)
                    CheckBoxPiccPollingLed.Checked = true;
                else
                    CheckBoxPiccPollingLed.Checked = false;

                if (behaviorSettings.piccActivationLed == LED_BUZZER_BEHAVIOR.ENABLED)
                    CheckBoxPiccActivationLed.Checked = true;
                else
                    CheckBoxPiccActivationLed.Checked = false;               

                if (behaviorSettings.cardInsertionRemovalBuzzer == LED_BUZZER_BEHAVIOR.ENABLED)
                    CheckBoxPiccCardInsertionBuzzer.Checked = true;
                else
                    CheckBoxPiccCardInsertionBuzzer.Checked = false;

                if(behaviorSettings.pn512ResetIndicationBuzzer == LED_BUZZER_BEHAVIOR.ENABLED)
                    CheckBoxPiccPN512ResetIndicationBuzzer.Checked = true;
                else
                    CheckBoxPiccPN512ResetIndicationBuzzer.Checked = false;

                if (behaviorSettings.piccColorSelectRed == LED_BUZZER_BEHAVIOR.ENABLED)
                    CheckBoxPiccColorSelectRed.Checked = true;
                else
                    CheckBoxPiccColorSelectRed.Checked = false;

                if (behaviorSettings.piccColorSelectGreen == LED_BUZZER_BEHAVIOR.ENABLED)
                    CheckBoxPiccColorSelectGreen.Checked = true;
                else
                    CheckBoxPiccColorSelectGreen.Checked = false;
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonSetOperatingParameter_Click(object sender, EventArgs e)
        {
            PiccOperatingParameter parameter;

            if (_nfcReader == null)
            {
                showWarningMessage("Please connect ACR1252U reader");
                return;
            }

            addTitleToLog(false, "Set PICC Operating Parameter");

            try
            {
                parameter = new PiccOperatingParameter();

                if (CheckBoxTypeA.Checked)
                    parameter.iso14443TypeA = PiccOperatingParameter.PARAMETER_OPTION.DETECT;

                if (CheckBoxTypeB.Checked)
                    parameter.iso14443TypeB = PiccOperatingParameter.PARAMETER_OPTION.DETECT;

                if (CheckBoxFelica212.Checked)
                    parameter.felica212 = PiccOperatingParameter.PARAMETER_OPTION.DETECT;

                if (CheckBoxFelica424.Checked)
                    parameter.felica424 = PiccOperatingParameter.PARAMETER_OPTION.DETECT;

                if (CheckBoxTopaz.Checked)
                    parameter.topaz = PiccOperatingParameter.PARAMETER_OPTION.DETECT;

                parameter = _nfcReader.setPiccOperatingParameter(parameter);

                CheckBoxTypeA.Checked = false;
                CheckBoxTypeB.Checked = false;
                CheckBoxFelica212.Checked = false;
                CheckBoxFelica424.Checked = false;
                CheckBoxTopaz.Checked = false;

            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonReadOperatingParameter_Click(object sender, EventArgs e)
        {
            PiccOperatingParameter parameter;

            if (_nfcReader == null)
            {
                showWarningMessage("Please connect ACR1252U reader");
                return;
            }

            addTitleToLog(false, "Read PICC Operating Parameter");

            try
            {
                parameter = _nfcReader.readPiccOperatingParameter();

                if (parameter.iso14443TypeA == PiccOperatingParameter.PARAMETER_OPTION.DETECT)
                    CheckBoxTypeA.Checked = true;
                else
                    CheckBoxTypeA.Checked = false;

                if (parameter.iso14443TypeB == PiccOperatingParameter.PARAMETER_OPTION.DETECT)
                    CheckBoxTypeB.Checked = true;
                else
                    CheckBoxTypeB.Checked = false;

                if (parameter.felica212 == PiccOperatingParameter.PARAMETER_OPTION.DETECT)
                    CheckBoxFelica212.Checked = true;
                else
                    CheckBoxFelica212.Checked = false;

                if (parameter.felica424 == PiccOperatingParameter.PARAMETER_OPTION.DETECT)
                    CheckBoxFelica424.Checked = true;
                else
                    CheckBoxFelica424.Checked = false;

                if (parameter.topaz == PiccOperatingParameter.PARAMETER_OPTION.DETECT)
                    CheckBoxTopaz.Checked = true;
                else
                    CheckBoxTopaz.Checked = false;
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonClear_Click(object sender, EventArgs e)
        {
            RichTextBoxLogs.Clear();
        }

        private void ButtonReset_Click(object sender, EventArgs e)
        {
            resetApplication();
        }

        private void ButtonQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void ButtonSetSerial_Click(object sender, EventArgs e)
        {
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if (!r.IsMatch(TextBoxSerialNumber.Text))
            {
                showWarningMessage("Please key-in alphanumeric value for Serial Number.");
                return;
            }

            addTitleToLog(false, "Set Serial Number");

            try
            {
                _nfcReader.setSerialNumber(ASCIIEncoding.ASCII.GetBytes(TextBoxSerialNumber.Text));
                TextBoxSerialNumber.Text = "";
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonReadSerial_Click(object sender, EventArgs e)
        {
            byte[] serialNumber;

            addTitleToLog(false, "Read Serial Number");

            try
            {
                serialNumber = _nfcReader.readSerialNumber();
                TextBoxSerialNumber.Text = ASCIIEncoding.ASCII.GetString(serialNumber).Substring(5);
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonSetAndLock_Click(object sender, EventArgs e)
        {
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if (!r.IsMatch(TextBoxSerialNumber.Text))
            {
                showWarningMessage("Please key-in alphanumeric value for Serial Number.");
                return;
            }

            addTitleToLog(false, "Set and Lock Serial Number");

            try
            {
                 _nfcReader.setAndLockSerialNumber(ASCIIEncoding.ASCII.GetBytes(TextBoxSerialNumber.Text));
                 TextBoxSerialNumber.Text = "";
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void ButtonUnlock_Click(object sender, EventArgs e)
        {
            addTitleToLog(false, "Unlock Serial Number");

            try
            {
                _nfcReader.unlockSerialNumber();
            }
            catch (PcscException ex)
            {
                showPcscException(ex.Message);
            }
            catch (Exception ex)
            {
                showSystemException(ex.Message);
            }
        }

        private void NumericUpDownDuration_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.V)
            {
                // cancel the "paste" function
                e.SuppressKeyPress = true;
            }
        }
    }
}
