﻿namespace UsbNfcBasicDeviceProgramming
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.LabelSelectReader = new System.Windows.Forms.Label();
            this.ComboBoxReader = new System.Windows.Forms.ComboBox();
            this.ButtonInitialize = new System.Windows.Forms.Button();
            this.ButtonConnect = new System.Windows.Forms.Button();
            this.GroupBoxFirmware = new System.Windows.Forms.GroupBox();
            this.ButtonGetFirmware = new System.Windows.Forms.Button();
            this.LabelFirmware = new System.Windows.Forms.Label();
            this.RichTextBoxLogs = new System.Windows.Forms.RichTextBox();
            this.GroupBoxLed = new System.Windows.Forms.GroupBox();
            this.ButtonGetLedStatus = new System.Windows.Forms.Button();
            this.ButtonSetLedStatus = new System.Windows.Forms.Button();
            this.CheckBoxGreenLed = new System.Windows.Forms.CheckBox();
            this.CheckBoxRedLed = new System.Windows.Forms.CheckBox();
            this.GroupBoxBuzzerControl = new System.Windows.Forms.GroupBox();
            this.ButtonGetBuzzerStatus = new System.Windows.Forms.Button();
            this.ButtonSetBuzzerStatus = new System.Windows.Forms.Button();
            this.LabelMultiplier = new System.Windows.Forms.Label();
            this.NumericUpDownDuration = new System.Windows.Forms.NumericUpDown();
            this.LabelDuration = new System.Windows.Forms.Label();
            this.GroupBoxPiccOperatingParameter = new System.Windows.Forms.GroupBox();
            this.ButtonReadOperatingParameter = new System.Windows.Forms.Button();
            this.ButtonSetOperatingParameter = new System.Windows.Forms.Button();
            this.CheckBoxTopaz = new System.Windows.Forms.CheckBox();
            this.CheckBoxFelica424 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFelica212 = new System.Windows.Forms.CheckBox();
            this.CheckBoxTypeB = new System.Windows.Forms.CheckBox();
            this.CheckBoxTypeA = new System.Windows.Forms.CheckBox();
            this.GroupBoxLedBuzzerBehaviorPicc = new System.Windows.Forms.GroupBox();
            this.CheckBoxPiccColorSelectGreen = new System.Windows.Forms.CheckBox();
            this.CheckBoxPiccColorSelectRed = new System.Windows.Forms.CheckBox();
            this.ButtonReadBehaviorPicc = new System.Windows.Forms.Button();
            this.ButtonSetBehaviorPicc = new System.Windows.Forms.Button();
            this.CheckBoxPiccCardOperationLed = new System.Windows.Forms.CheckBox();
            this.CheckBoxPiccCardInsertionBuzzer = new System.Windows.Forms.CheckBox();
            this.CheckBoxPiccActivationLed = new System.Windows.Forms.CheckBox();
            this.CheckBoxPiccPollingLed = new System.Windows.Forms.CheckBox();
            this.CheckBoxPiccPN512ResetIndicationBuzzer = new System.Windows.Forms.CheckBox();
            this.ButtonClear = new System.Windows.Forms.Button();
            this.ButtonReset = new System.Windows.Forms.Button();
            this.ButtonQuit = new System.Windows.Forms.Button();
            this.LabelLogs = new System.Windows.Forms.Label();
            this.GroupBoxSerialNumber = new System.Windows.Forms.GroupBox();
            this.ButtonUnlock = new System.Windows.Forms.Button();
            this.ButtonSetAndLock = new System.Windows.Forms.Button();
            this.ButtonReadSerial = new System.Windows.Forms.Button();
            this.ButtonSetSerial = new System.Windows.Forms.Button();
            this.TextBoxSerialNumber = new System.Windows.Forms.TextBox();
            this.GroupBoxFirmware.SuspendLayout();
            this.GroupBoxLed.SuspendLayout();
            this.GroupBoxBuzzerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownDuration)).BeginInit();
            this.GroupBoxPiccOperatingParameter.SuspendLayout();
            this.GroupBoxLedBuzzerBehaviorPicc.SuspendLayout();
            this.GroupBoxSerialNumber.SuspendLayout();
            this.SuspendLayout();
            // 
            // LabelSelectReader
            // 
            this.LabelSelectReader.AutoSize = true;
            this.LabelSelectReader.Location = new System.Drawing.Point(9, 9);
            this.LabelSelectReader.Name = "LabelSelectReader";
            this.LabelSelectReader.Size = new System.Drawing.Size(100, 16);
            this.LabelSelectReader.TabIndex = 0;
            this.LabelSelectReader.Text = "Select Reader";
            // 
            // ComboBoxReader
            // 
            this.ComboBoxReader.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxReader.FormattingEnabled = true;
            this.ComboBoxReader.Location = new System.Drawing.Point(12, 28);
            this.ComboBoxReader.Name = "ComboBoxReader";
            this.ComboBoxReader.Size = new System.Drawing.Size(268, 24);
            this.ComboBoxReader.TabIndex = 1;
            // 
            // ButtonInitialize
            // 
            this.ButtonInitialize.Location = new System.Drawing.Point(160, 58);
            this.ButtonInitialize.Name = "ButtonInitialize";
            this.ButtonInitialize.Size = new System.Drawing.Size(120, 26);
            this.ButtonInitialize.TabIndex = 2;
            this.ButtonInitialize.Text = "Initialize";
            this.ButtonInitialize.UseVisualStyleBackColor = true;
            this.ButtonInitialize.Click += new System.EventHandler(this.ButtonInitialize_Click);
            // 
            // ButtonConnect
            // 
            this.ButtonConnect.Location = new System.Drawing.Point(160, 90);
            this.ButtonConnect.Name = "ButtonConnect";
            this.ButtonConnect.Size = new System.Drawing.Size(120, 26);
            this.ButtonConnect.TabIndex = 3;
            this.ButtonConnect.Text = "Connect";
            this.ButtonConnect.UseVisualStyleBackColor = true;
            this.ButtonConnect.Click += new System.EventHandler(this.ButtonConnect_Click);
            // 
            // GroupBoxFirmware
            // 
            this.GroupBoxFirmware.Controls.Add(this.ButtonGetFirmware);
            this.GroupBoxFirmware.Controls.Add(this.LabelFirmware);
            this.GroupBoxFirmware.Location = new System.Drawing.Point(12, 118);
            this.GroupBoxFirmware.Name = "GroupBoxFirmware";
            this.GroupBoxFirmware.Size = new System.Drawing.Size(268, 95);
            this.GroupBoxFirmware.TabIndex = 4;
            this.GroupBoxFirmware.TabStop = false;
            this.GroupBoxFirmware.Text = "Firmware";
            // 
            // ButtonGetFirmware
            // 
            this.ButtonGetFirmware.Location = new System.Drawing.Point(135, 57);
            this.ButtonGetFirmware.Name = "ButtonGetFirmware";
            this.ButtonGetFirmware.Size = new System.Drawing.Size(120, 26);
            this.ButtonGetFirmware.TabIndex = 5;
            this.ButtonGetFirmware.Text = "Get Firmware";
            this.ButtonGetFirmware.UseVisualStyleBackColor = true;
            this.ButtonGetFirmware.Click += new System.EventHandler(this.ButtonGetFirmware_Click);
            // 
            // LabelFirmware
            // 
            this.LabelFirmware.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelFirmware.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelFirmware.Location = new System.Drawing.Point(10, 25);
            this.LabelFirmware.Name = "LabelFirmware";
            this.LabelFirmware.Size = new System.Drawing.Size(243, 24);
            this.LabelFirmware.TabIndex = 38;
            this.LabelFirmware.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RichTextBoxLogs
            // 
            this.RichTextBoxLogs.Location = new System.Drawing.Point(292, 235);
            this.RichTextBoxLogs.Name = "RichTextBoxLogs";
            this.RichTextBoxLogs.Size = new System.Drawing.Size(268, 535);
            this.RichTextBoxLogs.TabIndex = 5;
            this.RichTextBoxLogs.Text = "";
            // 
            // GroupBoxLed
            // 
            this.GroupBoxLed.Controls.Add(this.ButtonGetLedStatus);
            this.GroupBoxLed.Controls.Add(this.ButtonSetLedStatus);
            this.GroupBoxLed.Controls.Add(this.CheckBoxGreenLed);
            this.GroupBoxLed.Controls.Add(this.CheckBoxRedLed);
            this.GroupBoxLed.Location = new System.Drawing.Point(12, 355);
            this.GroupBoxLed.Name = "GroupBoxLed";
            this.GroupBoxLed.Size = new System.Drawing.Size(268, 95);
            this.GroupBoxLed.TabIndex = 6;
            this.GroupBoxLed.TabStop = false;
            this.GroupBoxLed.Text = "LED Control";
            // 
            // ButtonGetLedStatus
            // 
            this.ButtonGetLedStatus.Location = new System.Drawing.Point(135, 57);
            this.ButtonGetLedStatus.Name = "ButtonGetLedStatus";
            this.ButtonGetLedStatus.Size = new System.Drawing.Size(120, 26);
            this.ButtonGetLedStatus.TabIndex = 40;
            this.ButtonGetLedStatus.Text = "Get Status";
            this.ButtonGetLedStatus.UseVisualStyleBackColor = true;
            this.ButtonGetLedStatus.Click += new System.EventHandler(this.ButtonGetLedStatus_Click);
            // 
            // ButtonSetLedStatus
            // 
            this.ButtonSetLedStatus.Location = new System.Drawing.Point(10, 57);
            this.ButtonSetLedStatus.Name = "ButtonSetLedStatus";
            this.ButtonSetLedStatus.Size = new System.Drawing.Size(120, 26);
            this.ButtonSetLedStatus.TabIndex = 39;
            this.ButtonSetLedStatus.Text = "Set Status";
            this.ButtonSetLedStatus.UseVisualStyleBackColor = true;
            this.ButtonSetLedStatus.Click += new System.EventHandler(this.ButtonSetLedStatus_Click);
            // 
            // CheckBoxGreenLed
            // 
            this.CheckBoxGreenLed.Location = new System.Drawing.Point(148, 25);
            this.CheckBoxGreenLed.Name = "CheckBoxGreenLed";
            this.CheckBoxGreenLed.Size = new System.Drawing.Size(105, 26);
            this.CheckBoxGreenLed.TabIndex = 1;
            this.CheckBoxGreenLed.Text = "Green LED";
            this.CheckBoxGreenLed.UseVisualStyleBackColor = true;
            // 
            // CheckBoxRedLed
            // 
            this.CheckBoxRedLed.Location = new System.Drawing.Point(29, 25);
            this.CheckBoxRedLed.Name = "CheckBoxRedLed";
            this.CheckBoxRedLed.Size = new System.Drawing.Size(105, 26);
            this.CheckBoxRedLed.TabIndex = 0;
            this.CheckBoxRedLed.Text = "Red LED";
            this.CheckBoxRedLed.UseVisualStyleBackColor = true;
            // 
            // GroupBoxBuzzerControl
            // 
            this.GroupBoxBuzzerControl.Controls.Add(this.ButtonGetBuzzerStatus);
            this.GroupBoxBuzzerControl.Controls.Add(this.ButtonSetBuzzerStatus);
            this.GroupBoxBuzzerControl.Controls.Add(this.LabelMultiplier);
            this.GroupBoxBuzzerControl.Controls.Add(this.NumericUpDownDuration);
            this.GroupBoxBuzzerControl.Controls.Add(this.LabelDuration);
            this.GroupBoxBuzzerControl.Location = new System.Drawing.Point(12, 458);
            this.GroupBoxBuzzerControl.Name = "GroupBoxBuzzerControl";
            this.GroupBoxBuzzerControl.Size = new System.Drawing.Size(268, 95);
            this.GroupBoxBuzzerControl.TabIndex = 7;
            this.GroupBoxBuzzerControl.TabStop = false;
            this.GroupBoxBuzzerControl.Text = "Buzzer Control";
            // 
            // ButtonGetBuzzerStatus
            // 
            this.ButtonGetBuzzerStatus.Location = new System.Drawing.Point(135, 57);
            this.ButtonGetBuzzerStatus.Name = "ButtonGetBuzzerStatus";
            this.ButtonGetBuzzerStatus.Size = new System.Drawing.Size(120, 26);
            this.ButtonGetBuzzerStatus.TabIndex = 42;
            this.ButtonGetBuzzerStatus.Text = "Get Status";
            this.ButtonGetBuzzerStatus.UseVisualStyleBackColor = true;
            this.ButtonGetBuzzerStatus.Click += new System.EventHandler(this.ButtonGetBuzzerStatus_Click);
            // 
            // ButtonSetBuzzerStatus
            // 
            this.ButtonSetBuzzerStatus.Location = new System.Drawing.Point(10, 57);
            this.ButtonSetBuzzerStatus.Name = "ButtonSetBuzzerStatus";
            this.ButtonSetBuzzerStatus.Size = new System.Drawing.Size(120, 26);
            this.ButtonSetBuzzerStatus.TabIndex = 41;
            this.ButtonSetBuzzerStatus.Text = "Set Status";
            this.ButtonSetBuzzerStatus.UseVisualStyleBackColor = true;
            this.ButtonSetBuzzerStatus.Click += new System.EventHandler(this.ButtonSetBuzzerStatus_Click);
            // 
            // LabelMultiplier
            // 
            this.LabelMultiplier.AutoSize = true;
            this.LabelMultiplier.Location = new System.Drawing.Point(140, 25);
            this.LabelMultiplier.Name = "LabelMultiplier";
            this.LabelMultiplier.Size = new System.Drawing.Size(59, 16);
            this.LabelMultiplier.TabIndex = 2;
            this.LabelMultiplier.Text = "x 10 ms";
            // 
            // NumericUpDownDuration
            // 
            this.NumericUpDownDuration.Location = new System.Drawing.Point(80, 23);
            this.NumericUpDownDuration.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.NumericUpDownDuration.Name = "NumericUpDownDuration";
            this.NumericUpDownDuration.Size = new System.Drawing.Size(53, 23);
            this.NumericUpDownDuration.TabIndex = 1;
            this.NumericUpDownDuration.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericUpDownDuration_KeyDown);
            // 
            // LabelDuration
            // 
            this.LabelDuration.AutoSize = true;
            this.LabelDuration.Location = new System.Drawing.Point(10, 25);
            this.LabelDuration.Name = "LabelDuration";
            this.LabelDuration.Size = new System.Drawing.Size(69, 16);
            this.LabelDuration.TabIndex = 0;
            this.LabelDuration.Text = "Duration:";
            // 
            // GroupBoxPiccOperatingParameter
            // 
            this.GroupBoxPiccOperatingParameter.Controls.Add(this.ButtonReadOperatingParameter);
            this.GroupBoxPiccOperatingParameter.Controls.Add(this.ButtonSetOperatingParameter);
            this.GroupBoxPiccOperatingParameter.Controls.Add(this.CheckBoxTopaz);
            this.GroupBoxPiccOperatingParameter.Controls.Add(this.CheckBoxFelica424);
            this.GroupBoxPiccOperatingParameter.Controls.Add(this.CheckBoxFelica212);
            this.GroupBoxPiccOperatingParameter.Controls.Add(this.CheckBoxTypeB);
            this.GroupBoxPiccOperatingParameter.Controls.Add(this.CheckBoxTypeA);
            this.GroupBoxPiccOperatingParameter.Location = new System.Drawing.Point(292, 9);
            this.GroupBoxPiccOperatingParameter.Name = "GroupBoxPiccOperatingParameter";
            this.GroupBoxPiccOperatingParameter.Size = new System.Drawing.Size(268, 190);
            this.GroupBoxPiccOperatingParameter.TabIndex = 8;
            this.GroupBoxPiccOperatingParameter.TabStop = false;
            this.GroupBoxPiccOperatingParameter.Text = "PICC Operating Parameter";
            // 
            // ButtonReadOperatingParameter
            // 
            this.ButtonReadOperatingParameter.Location = new System.Drawing.Point(135, 150);
            this.ButtonReadOperatingParameter.Name = "ButtonReadOperatingParameter";
            this.ButtonReadOperatingParameter.Size = new System.Drawing.Size(120, 26);
            this.ButtonReadOperatingParameter.TabIndex = 49;
            this.ButtonReadOperatingParameter.Text = "Read Parameter";
            this.ButtonReadOperatingParameter.UseVisualStyleBackColor = true;
            this.ButtonReadOperatingParameter.Click += new System.EventHandler(this.ButtonReadOperatingParameter_Click);
            // 
            // ButtonSetOperatingParameter
            // 
            this.ButtonSetOperatingParameter.Location = new System.Drawing.Point(10, 150);
            this.ButtonSetOperatingParameter.Name = "ButtonSetOperatingParameter";
            this.ButtonSetOperatingParameter.Size = new System.Drawing.Size(120, 26);
            this.ButtonSetOperatingParameter.TabIndex = 48;
            this.ButtonSetOperatingParameter.Text = "Set Parameter";
            this.ButtonSetOperatingParameter.UseVisualStyleBackColor = true;
            this.ButtonSetOperatingParameter.Click += new System.EventHandler(this.ButtonSetOperatingParameter_Click);
            // 
            // CheckBoxTopaz
            // 
            this.CheckBoxTopaz.AutoSize = true;
            this.CheckBoxTopaz.Location = new System.Drawing.Point(10, 125);
            this.CheckBoxTopaz.Name = "CheckBoxTopaz";
            this.CheckBoxTopaz.Size = new System.Drawing.Size(67, 20);
            this.CheckBoxTopaz.TabIndex = 4;
            this.CheckBoxTopaz.Text = "Topaz";
            this.CheckBoxTopaz.UseVisualStyleBackColor = true;
            // 
            // CheckBoxFelica424
            // 
            this.CheckBoxFelica424.AutoSize = true;
            this.CheckBoxFelica424.Location = new System.Drawing.Point(10, 100);
            this.CheckBoxFelica424.Name = "CheckBoxFelica424";
            this.CheckBoxFelica424.Size = new System.Drawing.Size(131, 20);
            this.CheckBoxFelica424.TabIndex = 3;
            this.CheckBoxFelica424.Text = "FeliCa 424 Kbps";
            this.CheckBoxFelica424.UseVisualStyleBackColor = true;
            // 
            // CheckBoxFelica212
            // 
            this.CheckBoxFelica212.AutoSize = true;
            this.CheckBoxFelica212.Location = new System.Drawing.Point(10, 75);
            this.CheckBoxFelica212.Name = "CheckBoxFelica212";
            this.CheckBoxFelica212.Size = new System.Drawing.Size(131, 20);
            this.CheckBoxFelica212.TabIndex = 2;
            this.CheckBoxFelica212.Text = "FeliCa 212 Kbps";
            this.CheckBoxFelica212.UseVisualStyleBackColor = true;
            // 
            // CheckBoxTypeB
            // 
            this.CheckBoxTypeB.AutoSize = true;
            this.CheckBoxTypeB.Location = new System.Drawing.Point(10, 50);
            this.CheckBoxTypeB.Name = "CheckBoxTypeB";
            this.CheckBoxTypeB.Size = new System.Drawing.Size(147, 20);
            this.CheckBoxTypeB.TabIndex = 1;
            this.CheckBoxTypeB.Text = "ISO 14443 Type B";
            this.CheckBoxTypeB.UseVisualStyleBackColor = true;
            // 
            // CheckBoxTypeA
            // 
            this.CheckBoxTypeA.AutoSize = true;
            this.CheckBoxTypeA.Location = new System.Drawing.Point(10, 25);
            this.CheckBoxTypeA.Name = "CheckBoxTypeA";
            this.CheckBoxTypeA.Size = new System.Drawing.Size(148, 20);
            this.CheckBoxTypeA.TabIndex = 0;
            this.CheckBoxTypeA.Text = "ISO 14443 Type A";
            this.CheckBoxTypeA.UseVisualStyleBackColor = true;
            // 
            // GroupBoxLedBuzzerBehaviorPicc
            // 
            this.GroupBoxLedBuzzerBehaviorPicc.Controls.Add(this.CheckBoxPiccColorSelectGreen);
            this.GroupBoxLedBuzzerBehaviorPicc.Controls.Add(this.CheckBoxPiccColorSelectRed);
            this.GroupBoxLedBuzzerBehaviorPicc.Controls.Add(this.ButtonReadBehaviorPicc);
            this.GroupBoxLedBuzzerBehaviorPicc.Controls.Add(this.ButtonSetBehaviorPicc);
            this.GroupBoxLedBuzzerBehaviorPicc.Controls.Add(this.CheckBoxPiccCardOperationLed);
            this.GroupBoxLedBuzzerBehaviorPicc.Controls.Add(this.CheckBoxPiccCardInsertionBuzzer);
            this.GroupBoxLedBuzzerBehaviorPicc.Controls.Add(this.CheckBoxPiccActivationLed);
            this.GroupBoxLedBuzzerBehaviorPicc.Controls.Add(this.CheckBoxPiccPollingLed);
            this.GroupBoxLedBuzzerBehaviorPicc.Controls.Add(this.CheckBoxPiccPN512ResetIndicationBuzzer);
            this.GroupBoxLedBuzzerBehaviorPicc.Location = new System.Drawing.Point(12, 563);
            this.GroupBoxLedBuzzerBehaviorPicc.Name = "GroupBoxLedBuzzerBehaviorPicc";
            this.GroupBoxLedBuzzerBehaviorPicc.Size = new System.Drawing.Size(268, 240);
            this.GroupBoxLedBuzzerBehaviorPicc.TabIndex = 9;
            this.GroupBoxLedBuzzerBehaviorPicc.TabStop = false;
            this.GroupBoxLedBuzzerBehaviorPicc.Text = "LED and Buzzer Behavior for PICC";
            // 
            // CheckBoxPiccColorSelectGreen
            // 
            this.CheckBoxPiccColorSelectGreen.AutoSize = true;
            this.CheckBoxPiccColorSelectGreen.Location = new System.Drawing.Point(10, 175);
            this.CheckBoxPiccColorSelectGreen.Name = "CheckBoxPiccColorSelectGreen";
            this.CheckBoxPiccColorSelectGreen.Size = new System.Drawing.Size(162, 20);
            this.CheckBoxPiccColorSelectGreen.TabIndex = 6;
            this.CheckBoxPiccColorSelectGreen.Text = "Color Select (Green)";
            this.CheckBoxPiccColorSelectGreen.UseVisualStyleBackColor = true;
            // 
            // CheckBoxPiccColorSelectRed
            // 
            this.CheckBoxPiccColorSelectRed.AutoSize = true;
            this.CheckBoxPiccColorSelectRed.Location = new System.Drawing.Point(10, 150);
            this.CheckBoxPiccColorSelectRed.Name = "CheckBoxPiccColorSelectRed";
            this.CheckBoxPiccColorSelectRed.Size = new System.Drawing.Size(148, 20);
            this.CheckBoxPiccColorSelectRed.TabIndex = 5;
            this.CheckBoxPiccColorSelectRed.Text = "Color Select (Red)";
            this.CheckBoxPiccColorSelectRed.UseVisualStyleBackColor = true;
            // 
            // ButtonReadBehaviorPicc
            // 
            this.ButtonReadBehaviorPicc.Location = new System.Drawing.Point(135, 200);
            this.ButtonReadBehaviorPicc.Name = "ButtonReadBehaviorPicc";
            this.ButtonReadBehaviorPicc.Size = new System.Drawing.Size(120, 26);
            this.ButtonReadBehaviorPicc.TabIndex = 45;
            this.ButtonReadBehaviorPicc.Text = "Read Behavior";
            this.ButtonReadBehaviorPicc.UseVisualStyleBackColor = true;
            this.ButtonReadBehaviorPicc.Click += new System.EventHandler(this.ButtonReadBehaviorPicc_Click);
            // 
            // ButtonSetBehaviorPicc
            // 
            this.ButtonSetBehaviorPicc.Location = new System.Drawing.Point(10, 200);
            this.ButtonSetBehaviorPicc.Name = "ButtonSetBehaviorPicc";
            this.ButtonSetBehaviorPicc.Size = new System.Drawing.Size(120, 26);
            this.ButtonSetBehaviorPicc.TabIndex = 44;
            this.ButtonSetBehaviorPicc.Text = "Set Behavior";
            this.ButtonSetBehaviorPicc.UseVisualStyleBackColor = true;
            this.ButtonSetBehaviorPicc.Click += new System.EventHandler(this.ButtonSetBehaviorPicc_Click);
            // 
            // CheckBoxPiccCardOperationLed
            // 
            this.CheckBoxPiccCardOperationLed.AutoSize = true;
            this.CheckBoxPiccCardOperationLed.Location = new System.Drawing.Point(10, 25);
            this.CheckBoxPiccCardOperationLed.Name = "CheckBoxPiccCardOperationLed";
            this.CheckBoxPiccCardOperationLed.Size = new System.Drawing.Size(208, 20);
            this.CheckBoxPiccCardOperationLed.TabIndex = 0;
            this.CheckBoxPiccCardOperationLed.Text = "Card Operation Blinking LED";
            this.CheckBoxPiccCardOperationLed.UseVisualStyleBackColor = true;
            // 
            // CheckBoxPiccCardInsertionBuzzer
            // 
            this.CheckBoxPiccCardInsertionBuzzer.AutoSize = true;
            this.CheckBoxPiccCardInsertionBuzzer.Location = new System.Drawing.Point(10, 100);
            this.CheckBoxPiccCardInsertionBuzzer.Name = "CheckBoxPiccCardInsertionBuzzer";
            this.CheckBoxPiccCardInsertionBuzzer.Size = new System.Drawing.Size(256, 20);
            this.CheckBoxPiccCardInsertionBuzzer.TabIndex = 3;
            this.CheckBoxPiccCardInsertionBuzzer.Text = "Card Insertion and Removal Buzzer";
            this.CheckBoxPiccCardInsertionBuzzer.UseVisualStyleBackColor = true;
            // 
            // CheckBoxPiccActivationLed
            // 
            this.CheckBoxPiccActivationLed.AutoSize = true;
            this.CheckBoxPiccActivationLed.Location = new System.Drawing.Point(10, 75);
            this.CheckBoxPiccActivationLed.Name = "CheckBoxPiccActivationLed";
            this.CheckBoxPiccActivationLed.Size = new System.Drawing.Size(208, 20);
            this.CheckBoxPiccActivationLed.TabIndex = 2;
            this.CheckBoxPiccActivationLed.Text = "PICC Activation Status LED";
            this.CheckBoxPiccActivationLed.UseVisualStyleBackColor = true;
            // 
            // CheckBoxPiccPollingLed
            // 
            this.CheckBoxPiccPollingLed.AutoSize = true;
            this.CheckBoxPiccPollingLed.Location = new System.Drawing.Point(10, 50);
            this.CheckBoxPiccPollingLed.Name = "CheckBoxPiccPollingLed";
            this.CheckBoxPiccPollingLed.Size = new System.Drawing.Size(182, 20);
            this.CheckBoxPiccPollingLed.TabIndex = 1;
            this.CheckBoxPiccPollingLed.Text = "PICC Polling Status LED";
            this.CheckBoxPiccPollingLed.UseVisualStyleBackColor = true;
            // 
            // CheckBoxPiccPN512ResetIndicationBuzzer
            // 
            this.CheckBoxPiccPN512ResetIndicationBuzzer.AutoSize = true;
            this.CheckBoxPiccPN512ResetIndicationBuzzer.Location = new System.Drawing.Point(10, 125);
            this.CheckBoxPiccPN512ResetIndicationBuzzer.Name = "CheckBoxPiccPN512ResetIndicationBuzzer";
            this.CheckBoxPiccPN512ResetIndicationBuzzer.Size = new System.Drawing.Size(228, 20);
            this.CheckBoxPiccPN512ResetIndicationBuzzer.TabIndex = 4;
            this.CheckBoxPiccPN512ResetIndicationBuzzer.Text = "PN512 Reset Indication Buzzer";
            this.CheckBoxPiccPN512ResetIndicationBuzzer.UseVisualStyleBackColor = true;
            // 
            // ButtonClear
            // 
            this.ButtonClear.Location = new System.Drawing.Point(292, 776);
            this.ButtonClear.Name = "ButtonClear";
            this.ButtonClear.Size = new System.Drawing.Size(85, 26);
            this.ButtonClear.TabIndex = 10;
            this.ButtonClear.Text = "Clear";
            this.ButtonClear.UseVisualStyleBackColor = true;
            this.ButtonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // ButtonReset
            // 
            this.ButtonReset.Location = new System.Drawing.Point(383, 776);
            this.ButtonReset.Name = "ButtonReset";
            this.ButtonReset.Size = new System.Drawing.Size(85, 26);
            this.ButtonReset.TabIndex = 11;
            this.ButtonReset.Text = "Reset";
            this.ButtonReset.UseVisualStyleBackColor = true;
            this.ButtonReset.Click += new System.EventHandler(this.ButtonReset_Click);
            // 
            // ButtonQuit
            // 
            this.ButtonQuit.Location = new System.Drawing.Point(475, 776);
            this.ButtonQuit.Name = "ButtonQuit";
            this.ButtonQuit.Size = new System.Drawing.Size(85, 26);
            this.ButtonQuit.TabIndex = 12;
            this.ButtonQuit.Text = "Quit";
            this.ButtonQuit.UseVisualStyleBackColor = true;
            this.ButtonQuit.Click += new System.EventHandler(this.ButtonQuit_Click);
            // 
            // LabelLogs
            // 
            this.LabelLogs.AutoSize = true;
            this.LabelLogs.Location = new System.Drawing.Point(291, 215);
            this.LabelLogs.Name = "LabelLogs";
            this.LabelLogs.Size = new System.Drawing.Size(78, 16);
            this.LabelLogs.TabIndex = 13;
            this.LabelLogs.Text = "APDU Logs";
            // 
            // GroupBoxSerialNumber
            // 
            this.GroupBoxSerialNumber.Controls.Add(this.ButtonUnlock);
            this.GroupBoxSerialNumber.Controls.Add(this.ButtonSetAndLock);
            this.GroupBoxSerialNumber.Controls.Add(this.ButtonReadSerial);
            this.GroupBoxSerialNumber.Controls.Add(this.ButtonSetSerial);
            this.GroupBoxSerialNumber.Controls.Add(this.TextBoxSerialNumber);
            this.GroupBoxSerialNumber.Location = new System.Drawing.Point(12, 220);
            this.GroupBoxSerialNumber.Name = "GroupBoxSerialNumber";
            this.GroupBoxSerialNumber.Size = new System.Drawing.Size(268, 126);
            this.GroupBoxSerialNumber.TabIndex = 14;
            this.GroupBoxSerialNumber.TabStop = false;
            this.GroupBoxSerialNumber.Text = "Serial Number";
            // 
            // ButtonUnlock
            // 
            this.ButtonUnlock.Location = new System.Drawing.Point(135, 86);
            this.ButtonUnlock.Name = "ButtonUnlock";
            this.ButtonUnlock.Size = new System.Drawing.Size(118, 26);
            this.ButtonUnlock.TabIndex = 4;
            this.ButtonUnlock.Text = "Unlock";
            this.ButtonUnlock.UseVisualStyleBackColor = true;
            this.ButtonUnlock.Click += new System.EventHandler(this.ButtonUnlock_Click);
            // 
            // ButtonSetAndLock
            // 
            this.ButtonSetAndLock.Location = new System.Drawing.Point(10, 86);
            this.ButtonSetAndLock.Name = "ButtonSetAndLock";
            this.ButtonSetAndLock.Size = new System.Drawing.Size(120, 26);
            this.ButtonSetAndLock.TabIndex = 3;
            this.ButtonSetAndLock.Text = "Set and Lock";
            this.ButtonSetAndLock.UseVisualStyleBackColor = true;
            this.ButtonSetAndLock.Click += new System.EventHandler(this.ButtonSetAndLock_Click);
            // 
            // ButtonReadSerial
            // 
            this.ButtonReadSerial.Location = new System.Drawing.Point(135, 52);
            this.ButtonReadSerial.Name = "ButtonReadSerial";
            this.ButtonReadSerial.Size = new System.Drawing.Size(120, 26);
            this.ButtonReadSerial.TabIndex = 2;
            this.ButtonReadSerial.Text = "Read";
            this.ButtonReadSerial.UseVisualStyleBackColor = true;
            this.ButtonReadSerial.Click += new System.EventHandler(this.ButtonReadSerial_Click);
            // 
            // ButtonSetSerial
            // 
            this.ButtonSetSerial.Location = new System.Drawing.Point(10, 52);
            this.ButtonSetSerial.Name = "ButtonSetSerial";
            this.ButtonSetSerial.Size = new System.Drawing.Size(120, 26);
            this.ButtonSetSerial.TabIndex = 1;
            this.ButtonSetSerial.Text = "Set";
            this.ButtonSetSerial.UseVisualStyleBackColor = true;
            this.ButtonSetSerial.Click += new System.EventHandler(this.ButtonSetSerial_Click);
            // 
            // TextBoxSerialNumber
            // 
            this.TextBoxSerialNumber.Location = new System.Drawing.Point(10, 23);
            this.TextBoxSerialNumber.MaxLength = 20;
            this.TextBoxSerialNumber.Name = "TextBoxSerialNumber";
            this.TextBoxSerialNumber.Size = new System.Drawing.Size(245, 23);
            this.TextBoxSerialNumber.TabIndex = 0;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 808);
            this.Controls.Add(this.GroupBoxSerialNumber);
            this.Controls.Add(this.LabelLogs);
            this.Controls.Add(this.ButtonQuit);
            this.Controls.Add(this.ButtonReset);
            this.Controls.Add(this.ButtonClear);
            this.Controls.Add(this.GroupBoxLedBuzzerBehaviorPicc);
            this.Controls.Add(this.GroupBoxPiccOperatingParameter);
            this.Controls.Add(this.GroupBoxBuzzerControl);
            this.Controls.Add(this.GroupBoxLed);
            this.Controls.Add(this.RichTextBoxLogs);
            this.Controls.Add(this.GroupBoxFirmware);
            this.Controls.Add(this.ButtonConnect);
            this.Controls.Add(this.ButtonInitialize);
            this.Controls.Add(this.ComboBoxReader);
            this.Controls.Add(this.LabelSelectReader);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ACR1252U Basic Device Programming";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.GroupBoxFirmware.ResumeLayout(false);
            this.GroupBoxLed.ResumeLayout(false);
            this.GroupBoxBuzzerControl.ResumeLayout(false);
            this.GroupBoxBuzzerControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownDuration)).EndInit();
            this.GroupBoxPiccOperatingParameter.ResumeLayout(false);
            this.GroupBoxPiccOperatingParameter.PerformLayout();
            this.GroupBoxLedBuzzerBehaviorPicc.ResumeLayout(false);
            this.GroupBoxLedBuzzerBehaviorPicc.PerformLayout();
            this.GroupBoxSerialNumber.ResumeLayout(false);
            this.GroupBoxSerialNumber.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelSelectReader;
        private System.Windows.Forms.ComboBox ComboBoxReader;
        private System.Windows.Forms.Button ButtonInitialize;
        private System.Windows.Forms.Button ButtonConnect;
        private System.Windows.Forms.GroupBox GroupBoxFirmware;
        private System.Windows.Forms.Button ButtonGetFirmware;
        private System.Windows.Forms.Label LabelFirmware;
        private System.Windows.Forms.RichTextBox RichTextBoxLogs;
        private System.Windows.Forms.GroupBox GroupBoxLed;
        private System.Windows.Forms.CheckBox CheckBoxGreenLed;
        private System.Windows.Forms.CheckBox CheckBoxRedLed;
        private System.Windows.Forms.Button ButtonGetLedStatus;
        private System.Windows.Forms.Button ButtonSetLedStatus;
        private System.Windows.Forms.GroupBox GroupBoxBuzzerControl;
        private System.Windows.Forms.Label LabelDuration;
        private System.Windows.Forms.Button ButtonGetBuzzerStatus;
        private System.Windows.Forms.Button ButtonSetBuzzerStatus;
        private System.Windows.Forms.Label LabelMultiplier;
        private System.Windows.Forms.NumericUpDown NumericUpDownDuration;
        private System.Windows.Forms.GroupBox GroupBoxPiccOperatingParameter;
        private System.Windows.Forms.CheckBox CheckBoxTopaz;
        private System.Windows.Forms.CheckBox CheckBoxFelica424;
        private System.Windows.Forms.CheckBox CheckBoxFelica212;
        private System.Windows.Forms.CheckBox CheckBoxTypeB;
        private System.Windows.Forms.CheckBox CheckBoxTypeA;
        private System.Windows.Forms.Button ButtonReadOperatingParameter;
        private System.Windows.Forms.Button ButtonSetOperatingParameter;
        private System.Windows.Forms.GroupBox GroupBoxLedBuzzerBehaviorPicc;
        private System.Windows.Forms.Button ButtonReadBehaviorPicc;
        private System.Windows.Forms.Button ButtonSetBehaviorPicc;
        private System.Windows.Forms.CheckBox CheckBoxPiccCardOperationLed;
        private System.Windows.Forms.CheckBox CheckBoxPiccCardInsertionBuzzer;
        private System.Windows.Forms.CheckBox CheckBoxPiccActivationLed;
        private System.Windows.Forms.CheckBox CheckBoxPiccPollingLed;
        private System.Windows.Forms.CheckBox CheckBoxPiccPN512ResetIndicationBuzzer;
        private System.Windows.Forms.Button ButtonClear;
        private System.Windows.Forms.Button ButtonReset;
        private System.Windows.Forms.Button ButtonQuit;
        private System.Windows.Forms.Label LabelLogs;
        private System.Windows.Forms.CheckBox CheckBoxPiccColorSelectRed;
        private System.Windows.Forms.CheckBox CheckBoxPiccColorSelectGreen;
        private System.Windows.Forms.GroupBox GroupBoxSerialNumber;
        private System.Windows.Forms.Button ButtonUnlock;
        private System.Windows.Forms.Button ButtonSetAndLock;
        private System.Windows.Forms.Button ButtonReadSerial;
        private System.Windows.Forms.Button ButtonSetSerial;
        private System.Windows.Forms.TextBox TextBoxSerialNumber;
    }
}

